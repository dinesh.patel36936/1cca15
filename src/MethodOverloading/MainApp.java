package MethodOverloading;

import java.util.Scanner;

public class MainApp {
    public static void main(String[] args) {
        Student s1 = new Student();
        Scanner sc = new Scanner(System.in);
        System.out.println("SELECT SEARCH CRITERIA:");
        System.out.println("1: SEARCH BY NAME");
        System.out.println("2: SEARCH BY CONTACT");
        int choice = sc.nextInt();
        if(choice==1)
        {
            System.out.println("Enter Name:");
            String name = sc.next();
            s1.search(name);
        } else if (choice==2) {
            System.out.println("Enter Contact:");
            int contact = sc.nextInt();
            s1.search(contact);
        }else {
            System.out.println("Invalid choice");
        }
    }
}
