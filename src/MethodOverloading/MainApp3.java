package MethodOverloading;

public class MainApp3 {
    public static void main(String[] args) {
        System.out.println("Main Method");
        main(25);
        main('j');
    }

    public static void main(int a) {
        System.out.println(a);
        System.out.println("Main Method2");
    }

    public static void main(char c) {
        System.out.println(c);
        System.out.println("Main Method3");
    }
}
