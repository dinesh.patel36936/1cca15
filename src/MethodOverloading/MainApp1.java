package MethodOverloading;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Facebook b1 = new Facebook();
        System.out.println("1:Enter Email:");
        System.out.println("2:Enter Contact:");
        int choice = sc.nextInt();
        if(choice==1)
        {
            System.out.println("Enter Email Id:");
            String email = sc.next();
            System.out.println("Enter Password:");
            String pass = sc.next();
            b1.login(email,pass);
        }
        else if(choice==2)
        {
            System.out.println("Enter Contact Number:");
            long contact = sc.nextLong();
            System.out.println("Enter Password:");
            String pass = sc.next();
            b1.login(contact,pass);
        }else {
            System.out.println("Invalid Choice");
        }
    }
}
