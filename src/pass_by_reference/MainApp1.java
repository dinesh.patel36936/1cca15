package pass_by_reference;

public class MainApp1 {
    public static void main(String[] args) {
        Company c1 = new Company();
        c1.scheduleInterview(new Employee());
    }
}
