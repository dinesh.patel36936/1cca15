package pass_by_reference;

public class Company {
    void scheduleInterview(Employee e)
    {
        if(e.empId==150)
        {
            System.out.println("Interview Scheduled");
        }
        else {
            System.out.println("Invalid reference");
        }
    }
}
