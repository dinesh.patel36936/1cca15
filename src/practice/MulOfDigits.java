package practice;

public class MulOfDigits {
    public static void main(String[] args) {
        int n = 1234;
        int mul = 1;
        while(n!=0)
        {
            int r = n%10;
            mul*=r;
            n/=10;
        }
        System.out.println(mul);
    }
}
