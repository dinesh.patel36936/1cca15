package practice;

public class p2 {
    public static void main(String[] args) {
        int line = 5;
        int star = 7;
        int space = 0;
        for (int i = 0; i < line-2; i++) {
            for (int j = 0; j <space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print("*");
            }
            System.out.println();
            star-=2;
            space++;
        }
        star = 1;
        space = 3;
        for (int i = 0; i < line-1; i++) {
            for (int j = 0; j <space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print("*");
            }
            System.out.println();
            star+=2;
            space--;
        }

    }
}
