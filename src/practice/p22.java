package practice;

public class p22 {
    public static void main(String[] args) {
        int line = 9;
        int star = 1;
        int space = 4;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print("*");
            }
            System.out.println();
            if(i<4)
            {
                space--;
                star+=2;
            }else
            {
                star-=2;
                space++;
            }
        }
    }
}
