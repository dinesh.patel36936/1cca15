package practice;

public class p24 {
    public static void main(String[] args) {
        int line = 10;
        int star = 9;
        int space1 = 3;
        int space2 = 6;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j<star; j++)
            {
                if(j>space1||j<space2)
                {
                    System.out.print("*");
                    space1--;
                    space2++;
                }else {
                    System.out.print("");
                }
            }
            System.out.println();
        }
    }
}
