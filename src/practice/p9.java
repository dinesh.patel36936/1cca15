package practice;

public class p9 {
    public static void main(String[] args) {
        int line = 8;
        int star = 5;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                System.out.print("*");
            }
            System.out.println();
            if(i<4)
            {
                star--;
            }
            else {
                star++;
            }
        }
    }
}
