package factorydesignpattern;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Select Account Type");
        System.out.println("1: Saving \n2: Loan");
        int accType = sc.nextInt();
        System.out.println("Enter account opening balance");
        double balance = sc.nextDouble();

        AccountFactory factory = new AccountFactory();
        Account accRef = factory.createAccount(accType, balance);

        boolean status = true;
         while(status)
         {
             System.out.println("Select mode of transanctions: ");
             System.out.println("1: Deposit\n2: Withdraw \n3: Check Balance\n4: Exit");
             int choice = sc.nextInt();
             if(choice==1)
             {
                 System.out.println("Enter Amount: ");
                 double amt = sc.nextDouble();
                 accRef.deposit(amt);
             }else if(choice==2)
             {
                 System.out.println("Enter Amount: ");
                 double amt = sc.nextDouble();
                 accRef.withdraw(amt);
             }else if(choice==3)
             {
                 accRef.checkBalance();
             }else if(choice==4)
             {
                 status = false;
             }
         }
    }
}
