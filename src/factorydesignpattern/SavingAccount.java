package factorydesignpattern;

public class SavingAccount implements  Account{
    public SavingAccount(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    private double accountBalance;

    @Override
    public void deposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt+"Rs credit to your account.");
    }

    @Override
    public void withdraw(double amt) {
        if(amt<=accountBalance)
        {
            accountBalance-=amt;
            System.out.println(amt+" Rs debited from your account.");
        }else {
            System.out.println("Insufficient Account Balance.");
        }
    }

    @Override
    public void checkBalance() {
        System.out.println("Active Balance "+accountBalance);
    }
}
