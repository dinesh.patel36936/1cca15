package factorydesignpattern;

public class LoanAccount implements Account{
    private double loanAmount;

    public LoanAccount(double loanAmount) {
        this.loanAmount = loanAmount;
    }

    @Override
    public void deposit(double amt) {
        loanAmount-=amt;
        System.out.println(amt+" Rs Debited from loan Account.");
    }

    @Override
    public void withdraw(double amt) {
        loanAmount+=amt;
        System.out.println(amt+ "Rs credited to your account.");
    }

    @Override
    public void checkBalance() {
        System.out.println("Active loan amount"+loanAmount);
    }
}
