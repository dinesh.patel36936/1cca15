package switchCase;

import java.util.Scanner;

public class SwitchDemo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Selected Language:");
        System.out.println(" 1:JAVA\n 2:PYTHON \n 3:PHP");
        int choice = sc.nextInt();
        switch(choice)
        {
            case 1:
                System.out.println("Selected Java");
                break;
            case 2:
                System.out.println("Selected Python");
                break;
            case 3:
                System.out.println("Selected PHP");
                break;
            default:
                System.out.println("Invalid Choice");
        }
    }
}
