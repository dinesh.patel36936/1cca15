package string;

import java.util.Scanner;

public class DuplicateCharacter {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your String:");
        String s1 = sc.nextLine();
        char[] s2 = s1.toCharArray();
        int count = 0;
        for (int i = 0; i < s2.length; i++) {
            count = 1;
            for (int j = i+1; j < s2.length; j++) {
                if(s2[i]==s2[j]&&s2[i]!=' ')
                {
                    count++;
                    s2[j]=' ';
                }
            }
            //find duplicate element in string
            if(count>1)
            {
                System.out.println(s2[i]);
            }

            //remove duplicate element
//            if(count<=1)
//            {
//                System.out.println(s2[i]);
//            }
        }
    }
}
