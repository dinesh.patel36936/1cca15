package string;

import java.util.Scanner;

public class String2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your String:");
        String str = sc.nextLine();
        String[] ch = str.split(" ");
        for (int i = 0; i < ch.length; i++) {
                char[] ch1 = ch[i].toCharArray();
                char t = ch1[0];
                int len = ch1.length;
                ch1[0] = ch1[len-1];
                ch1[len-1] = t;
                System.out.println(ch1);
        }
    }
}
