package string;

public class StringDemo2 {
    public static void main(String[] args) {
        String name = " Dinesh Patel ";
        System.out.println(name.length());
        System.out.println(name.charAt(9));
        System.out.println(name.indexOf('s'));
        System.out.println(name.lastIndexOf('e'));
        System.out.println(name.contains("Pat"));
        System.out.println(name.startsWith("D"));
        System.out.println(name.endsWith("l"));
        System.out.println(name.substring(0,6));
        System.out.println(name.substring(7));
        System.out.println(name.toLowerCase());
        System.out.println(name.toUpperCase());
        String[] s = name.split(" ");
        for (int i = 0; i < s.length; i++) {
            System.out.println(s[i]);
        }
        char[] ch = name.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            System.out.println(ch[i]);
        }
        System.out.println(name.isEmpty());
        String s2 = name.trim();
        System.out.println(s2);
    }
}
