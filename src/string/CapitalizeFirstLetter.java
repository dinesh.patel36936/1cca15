package string;

import java.util.Scanner;

public class CapitalizeFirstLetter {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your string:");
        String str = sc.nextLine();
        String[] ch = str.split(" ");
        for (int i = 0; i < ch.length; i++) {
            String c = ch[i];
            String ans = c.substring(0,1).toUpperCase()+c.substring(1);
            System.out.println(ans);
        }
    }
}
