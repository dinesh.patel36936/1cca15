package string;

public class StringDemo3 {
    public static void main(String[] args) {

        String s1 = "java";
        String s2 = "java";
        s1 = s1.toUpperCase();

        System.out.println(s1);
        System.out.println(s1.toLowerCase());
        System.out.println(s1.concat(" LOVES SQL"));
    }
}
