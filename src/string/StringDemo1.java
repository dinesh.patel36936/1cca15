package string;

public class StringDemo1 {
    public static void main(String[] args) {
        String S = "Software Developer";
        System.out.println(S.replace("Developer", "Programmer"));
        System.out.println(S.isEmpty());
        String[] ch = S.split("e");
        System.out.println(ch.length);
        for (int i = 0; i < ch.length; i++) {
            System.out.println(ch[i]);
        }
        System.out.println(S.length());
        System.out.println(S.charAt(9));
        System.out.println(S.indexOf('e'));
        System.out.println(S.lastIndexOf('e'));
        System.out.println(S.contains("eve"));
        System.out.println(S.startsWith("Soft"));
        System.out.println(S.endsWith("per"));
        System.out.println(S.substring(9));
        System.out.println(S.substring(0, 8));
        System.out.println(S.toUpperCase());
        System.out.println(S.toLowerCase());
        System.out.println(S.getBytes());
    }
}
