package whileLoop;

import java.util.Scanner;

public class WhileLoopDemo2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int no=0, sum = 0;
        while(sum<=50)
        {
            System.out.println("Enter no:");
            no = sc.nextInt();
            sum = sum+no;
        }
        System.out.println("Sum is:"+sum);
    }
}
