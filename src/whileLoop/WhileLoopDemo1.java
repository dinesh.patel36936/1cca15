package whileLoop;

import java.util.Scanner;

public class WhileLoopDemo1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int no = 0;
        while(no>=0)
        {
            System.out.println("Enter No:");
            no = sc.nextInt();
            System.out.println(no);
        }
        System.out.println("Entered the negetive value.");
    }
}
