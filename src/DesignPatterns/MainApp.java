package DesignPatterns;

public class MainApp {
    public static void main(String[] args) {
        GoogleAccount acc1 =  GoogleAccount.login();
        acc1.accessGmail();
        acc1.accessDrive();
        System.out.println(acc1);
    }
}
