package encapsulation;
@FunctionalInterface
public interface Shape {
    void area();
}
