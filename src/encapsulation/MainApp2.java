package encapsulation;

public class MainApp2 {
    public static void main(String[] args) {
        Mobile m1 = new Mobile();
        System.out.println("COMPANY NAME IS "+m1.company);

        //non-static
       // Mobile.RAM r1 = m1.new RAM();
       // r1.displayInfo();
        //Mobile.Processor P1 = m1.new Processor();
        //P1.displayName();

        //static
        Mobile.RAM r2 = new Mobile.RAM();
        r2.displayInfo();

        Mobile.Processor p1 = new Mobile.Processor();
        p1.displayName();




    }
}
