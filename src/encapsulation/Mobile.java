package encapsulation;

public class Mobile {
    String company = "SAMSUNG";

//    class RAM{
//        void displayInfo()
//        {
//            System.out.println("RAM SIZE IS 86 GB");
//        }
//    }
    static class RAM{
        void displayInfo()
        {
            System.out.println("RAM SIZE IS 86 GB");
        }
    }
//    class Processor{
//        void displayName()
//        {
//            System.out.println("PROCESSOR NAME IS SNAPDRAGON");
//        }
//    }
static class Processor{
       void displayName()
       {
          System.out.println("PROCESSOR NAME IS SNAPDRAGON");
      }
    }
}
