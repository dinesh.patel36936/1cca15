package encapsulation;

public class MainApp1 {
    public static void main(String[] args) {
        Employee e = new Employee();
        System.out.println(e.getEmpId());
        System.out.println(e.getEmpSalary());

        e.setEmpId(21);
        e.setEmpSalary(25000);
        System.out.println(e.getEmpId());
        System.out.println(e.getEmpSalary());
    }
}
