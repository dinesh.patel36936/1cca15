package encapsulation;

public class MainApp3 {
    public static void main(String[] args) {
        Shape s1 = new Shape() {
            @Override
            public void area() {
                double area = 3.14*4*5;
                System.out.println(area);
            }
        };
        s1.area();

        Shape s2 = new Shape() {
            @Override
            public void area() {
                double area = .5*3*4;
                System.out.println(area);
            }
        };
        s2.area();
    }
}
