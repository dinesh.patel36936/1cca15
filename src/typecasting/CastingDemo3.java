package typecasting;

public class CastingDemo3 {
    public static void main(String[] args) {
        short s1 = (short) 132569;//narrowing
        System.out.println(s1);//print garbage value

        long l1 = 896566263632656L;
        int x1 = (int) l1;//narrowing
        System.out.println(x1);//print garbage value

    }
}
