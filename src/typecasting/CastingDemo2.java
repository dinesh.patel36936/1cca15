package typecasting;

public class CastingDemo2 {
    public static void main(String[] args) {
        int x = 53;
        float y = 35.4f;
        System.out.println(x+"\t"+y);
        //narrowing
        int a = (int) y;
        //widening
        float b = x;
        System.out.println(a+"\t"+b);
    }
}
