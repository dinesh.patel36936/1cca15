package typecasting;

public class CastingDemo1 {
    public static void main(String[] args) {
        int a = 15;
        double b = 23.45;
        System.out.println(a+"\t"+b);
        //narrowing
        int c = (int) 35.85;
        //widening
        double d = 26;
        System.out.println(c+"\t"+d);
    }
}
