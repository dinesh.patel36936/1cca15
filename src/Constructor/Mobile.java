package Constructor;

public class Mobile extends Product{
    double productPrice = 35000;
    int qty = 10;
    void info()
    {
        System.out.println("Price:"+productPrice);
        System.out.println("Qty:"+qty);
        System.out.println("====================");
        System.out.println("Price:"+super.productPrice);
        System.out.println("Qty:"+super.qty);
    }
}
