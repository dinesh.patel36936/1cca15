package casting;

public class CastingDemo4 {
    public static void main(String[] args) {
        short s1 = (short) 132569;//narrowing
        System.out.println(s1);
        long l1 = 896562636326562l;
        int x1 = (int) l1;//narrowing
        System.out.println(x1);

    }
}
