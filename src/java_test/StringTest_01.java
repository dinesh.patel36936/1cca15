package java_test;
import  java.util.Scanner;
public class StringTest_01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your String:");
        String str = sc.nextLine();
        String s1 = str.substring(0,1);
        int n = str.length();
        String s2 = str.substring(n-1,n);
        String ans = str.replace(s1, s2);
        str = ans.substring(0,n-1)+s1;
        System.out.println(str);
    }
}
