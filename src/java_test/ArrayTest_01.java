package java_test;
import java.util.Scanner;
public class ArrayTest_01 {
    public static void main(String[] args) {
        int[] array = {1,2,3,4,5};
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your number:");
        int num =  sc.nextInt();
        boolean count = true;
        for (int i = 0; i <array.length ; i++) {
            if(array[i]==num)
            {
                System.out.println("Number is found in "+(i)+" position");
                count = false;
                break;
            }
        }
        if(count)
        {
            System.out.println("Number is not found in given array.");
        }
    }
}
