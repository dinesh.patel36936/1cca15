package java_test;

public class ArrayTest_02 {
    public static void main(String[] args) {
        int[] arr1 = {10, 20, 30};
        int[] arr2 = {40, 50, 60};
        int[] result = new int[arr1.length*2];
        int j = 0;

        for (int i = 0; i <arr1.length ; i++) {
            result[j] = arr1[i];
            j++;
        }
        for (int i = 0 ; i <arr2.length ; i++) {
            result[j] = arr2[i];
            j++;
        }
        for (int i = 0; i <arr1.length*2 ; i++) {
            System.out.println(result[i]+" ");

        }
    }
}
