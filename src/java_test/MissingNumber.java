package java_test;
import java.util.Scanner;
public class MissingNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your number:");
        int[] a = new int[5];
        for (int i = 0; i < 5; i++) {
            a[i] = sc.nextInt();
        }
        int n = missedNumber(a);
        System.out.println(n);
    }
    private  static int missedNumber(int[] a)
    {
        int ans = 0;
        int[] temp = new int[6];
        for (int i = 0; i <=5; i++) {
            temp[i] = 0;
        }
        for (int i = 0; i < 5; i++) {
            temp[a[i]-1] = 1;
        }
        for (int i = 0; i <=5; i++) {
            if(temp[i]==0)
            {
                ans = i+1;
            }
        }
        return  ans;
    }
}
