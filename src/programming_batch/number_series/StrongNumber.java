package programming_batch.number_series;

public class StrongNumber {
    public static void main(String[] args) {
        for (int i = 1; i <=100000 ; i++) {
            int  k = i;
            int sum = 0;
            while(k!=0)
            {
                int fact = 1;
                int r = k%10;
                for (int j = 1; j <=r ; j++) {
                    fact=fact*j;
                }
                sum = sum+fact;
                k/= 10;
            }
            if(i==sum)
            {
                System.out.println(i+" number is strong number");
            }

        }
    }
}
