package programming_batch.number_series;

import java.util.Scanner;

public class DuckNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your number:");
        String str = sc.nextLine();
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if(str.charAt(i)=='0')
            {
                count++;
            }
        }
        char st = str.charAt(0);
        if(st!='0'&&count>0)
        {
            System.out.println("Duck number");
        }else {
            System.out.println("Not Duck number");
        }
    }
}
