package programming_batch.number_series;

public class fibonacci2
{
 static int n1 = 0;
 static int n2 = 1;
    public static void main(String[] args) {
      fibonacci(35);
    }

    private static void fibonacci(int a) {
        if(a>0)
        {
            System.out.print(n1+" ");
            int n3 = n1+n2;
            n1=n2;
            n2=n3;
            a--;
            fibonacci(a);
        }
    }
}
