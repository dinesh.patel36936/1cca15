package programming_batch.number_series;

public class SpyNumber {
    public static void main(String[] args) {
        for (int i = 1; i<=100000 ; i++) {
            int n = i;
            int d = 0;
            int p = 1;
            while(n!=0)
            {
                int r = n%10;
                d+=r;
                p*=r;
                n/=10;
            }
            if(d==p)
            {
                System.out.println(i);
            }

        }
    }
}
