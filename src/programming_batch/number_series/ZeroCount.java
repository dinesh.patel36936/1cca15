package programming_batch.number_series;

public class ZeroCount {
    public static void main(String[] args) {
        for (int i = 1; i <=100 ; i++) {
            long a = i;
            int zero = 0;
            while(a!=0)
            {
                long r = a%10;
                if(r==0)
                {
                    zero++;
                }
                a/=10;
            }
            System.out.println(zero+" times zero occures in " + i+" Number");
        }
    }
}
