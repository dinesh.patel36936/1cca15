package programming_batch.number_series;

public class PrimeNumber {
    public static void main(String[] args) {
        int c = 0;
        for(int i = 1; i<=10000; i++)
        {
            int count = 0;
            for(int j = 1; j<=i; j++)
            {
                if(i%j==0)
                {
                    count++;
                }
            }
            if(count==2)
            {
                c++;
                //System.out.println(i+" number is prime number.");
            }
        }
        System.out.println("total prime number:"+c);
    }
}
