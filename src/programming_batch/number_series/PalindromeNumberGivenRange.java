package programming_batch.number_series;

public class PalindromeNumberGivenRange {
    public static void main(String[] args) {
        for (int i = 10; i <=10000 ; i++) {
            int n = i;
            int rev = 0;
            while(n!=0)
            {
                int r = n%10;
                rev = rev*10+r;
                n/=10;
            }
            if(i==rev)
            {
                System.out.println(i+" number is palidrome");
            }

        }
    }
}
