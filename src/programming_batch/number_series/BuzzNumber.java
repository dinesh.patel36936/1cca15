package programming_batch.number_series;

public class BuzzNumber {
    public static void main(String[] args) {
        for (int i = 1; i <=10000 ; i++) {
            int n = i;
            if(n%7==0||n%10==7)
            {
                System.out.println(i+" is buzz number");
            }
        }
    }
}
