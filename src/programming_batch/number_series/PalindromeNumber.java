package programming_batch.number_series;

public class PalindromeNumber {
    public static void main(String[] args) {
        int n = 111;
        int num = n;
        int res = 0;
        while(n!=0)
        {
            int r = n%10;
            res = res*10+r;
            n/=10;
        }
        if(res==num)
        {
            System.out.println("Palindrome");
        }else {
            System.out.println("Not Palindrome");
        }
    }
}
