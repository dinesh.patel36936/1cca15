package programming_batch.number_series;

public class ArmstrongNumber {
    public static void main(String[] args) {
        for (int i = 1; i <=10000 ; i++) {
                int k = i;
                int l = k;
                int n = 0;
                int sum = 0;
                while(k!=0)
                {
                    int r=k%10;
                    n++;
                    k/=10;
                }
                while(l!=0)
                {
                   int r = l%10;
                    int a = (int) Math.pow(r,n);
                    sum+=a;
                    l/=10;
                }
            if(i==sum)
            {
                System.out.println(i+" number is armstrong number");
            }
        }
    }
}
