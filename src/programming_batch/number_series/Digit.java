package programming_batch.number_series;

public class Digit {
    public static void main(String[] args) {
        int n = 1234;
        int res = 0;
        while(n!=0)
        {
            int r=n%10;
            res = res*10+r;
            n/=10;
        }
        System.out.println(res);
    }
}
