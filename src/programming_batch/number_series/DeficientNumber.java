package programming_batch.number_series;

import java.util.Scanner;

public class DeficientNumber {
    public static void main(String[] args) {
        for (int i = 1; i <= 100; i++) {
            int sum = 0;
            for (int j= 1; j <= i; j++) {
                if (i % j == 0) {
                    sum += j;
                }
            }
            if (sum < 2 * i) {
                System.out.println(i + " number is deficient number.");
            }
        }
    }

}
