package programming_batch.number_series;

import java.util.Scanner;

public class UglyNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your Number:");
            int j = sc.nextInt();
            int k = j;
            while(j!=1)
            {
                if(j%2==0)
                {
                    j/=2;
                }
                else if(j%3==0)
                {
                    j/=3;
                }
                else if(j%5==0)
                {
                    j/=5;
                }
                else {
                    System.out.println(k+" number is not Ugly number.");
                    return;
                }
            }
            System.out.println(k+" number is Ugly number");

    }
}
