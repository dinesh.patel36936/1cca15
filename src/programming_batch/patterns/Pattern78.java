package programming_batch.patterns;

public class Pattern78 {
    public static void main(String[] args) {
        int line = 5;
        int star = 10;
        int space = 0;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                if(j==0||j==star-1)
                {
                    System.out.print("N");
                }else {
                    System.out.print(" ");
                }

            }
            star-=2;
            space++;
            System.out.println();
        }
    }
}
