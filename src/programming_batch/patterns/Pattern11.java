package programming_batch.patterns;

public class Pattern11 {
    public static void main(String[] args) {
        int star1 = 5;
        int star2 = 5;
        int space = 0;
        int line = 5;
        for (int j = 0; j <line ; j++) {
            for (int i = 0; i < star1; i++) {
                System.out.print(" ");
            }
            for (int i = 0; i < space-1; i++) {
                System.out.print("*");
            }
            for (int i = 0; i < star2; i++) {
                System.out.print(" ");
            }
            star1--;
            star2--;
            space+=2;
            System.out.println();
        }
        star1 = 1;
        star2  = 1;
        space = 8;
        for (int j = 0; j <line ; j++) {
            for (int i = 0; i < star1; i++) {
                System.out.print(" ");
            }
            for (int i = 0; i < space-1; i++) {
                System.out.print("*");
            }
            for (int i = 0; i < star2; i++) {
                System.out.print(" ");
            }
            star1++;
            star2++;
            space-=2;
            System.out.println();
        }
    }
}
