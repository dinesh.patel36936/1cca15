package programming_batch.patterns;

public class Pattern110 {
    public static void main(String[] args) {
        int line = 5;
        int star1 = 4;
        int star2 = 1;
        int ch = 1;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star1; j++) {
                System.out.print("1"+" ");
            }
            for(int j = 0;j<star2; j++)
            {
                System.out.print(ch+" ");
            }
            ch++;
            star1--;
            star2++;
            System.out.println();
        }
    }
}
