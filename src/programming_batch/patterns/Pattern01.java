package programming_batch.patterns;

public class Pattern01 {
    public static void main(String[] args) {
        int line = 5;
        int star = 1;
        int space = 2;
        for (int i = 0; i <line ; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                if(j==0||j==star-1)
                {
                    System.out.print("*");
                }
                else {
                    System.out.print(" ");
                }
            }
            if(i<=1)
            {
                star+=2;
                space--;
            }
            else {
                space++;
                star-=2;
            }
            System.out.println();
        }
    }
}
