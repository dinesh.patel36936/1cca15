package programming_batch.patterns;

public class Pattern07 {
    public static void main(String[] args) {
        int star = 5;
        int space = 0;
        int line = 5;
        for (int i = 0; i < line-1; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print("* ");
            }
            star--;
            space++;
            System.out.println();
        }
        space = 4;
        star = 1;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print("* ");
            }
            star++;
            space--;
            System.out.println();
        }
    }
}
