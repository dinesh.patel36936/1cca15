package programming_batch.patterns;

public class Pattern03 {
    public static void main(String[] args) {
        int line = 6;
        int star = 1;
        int space = 4;
        int ch1 = 1;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                if(i==j)
                {
                    System.out.print(ch1);
                }
                else {
                    System.out.print("*");
                }
            }
            System.out.println();
            star+=2;
            space--;
            ch1++;
        }
    }
}
