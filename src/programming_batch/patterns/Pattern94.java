package programming_batch.patterns;

public class Pattern94 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                if(j==0||j==star-1||i==0)
                {
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }
            star--;
            System.out.println();
        }
    }
}
