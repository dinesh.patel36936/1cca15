package programming_batch.patterns;

public class Pattern91 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        int space = 1;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                if(j==0)
                {
                    System.out.print("N"+" ");
                }else {
                    System.out.print(" ");
                }
            }
            star--;
            space++;
            System.out.println();
        }
    }
}
