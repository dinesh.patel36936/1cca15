package programming_batch.patterns;

public class Pattern95 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                if(i==0||i==j||j==star-1){
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
