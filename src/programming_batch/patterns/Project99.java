package programming_batch.patterns;

public class Project99 {
    public static void main(String[] args) {
        int line = 9;
        int star = 1;
        for (int i = 0; i < line; i++) {

            for (int j = 0; j < star; j++) {
                if(j==0||j==star-1)
                {
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }
            System.out.println();
            if(i<4)
            {
                star+=2;
            }else
            {
                star-=2;
            }
        }
    }
}
