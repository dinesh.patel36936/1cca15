package programming_batch.patterns;

public class Pattern02 {
    public static void main(String[] args) {
        int line = 6;
        int star = 1;
        for (int i = 0; i <line ; i++) {

            for (int j = 0; j < star; j++) {
                if(j==star-1)
                {
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }
            System.out.println();
            if(i<=2)
            {
                star++;
            }else {
                star--;
            }
        }
    }
}
