package programming_batch.patterns;

public class Pattern04 {
    public static void main(String[] args) {
        int line = 7;
        int star = 1;
        int ch = 3;
        for (int i = 0; i <line ; i++) {
            for (int j = 0; j < star; j++) {
                System.out.print(ch--+" ");
            }
            System.out.println();
            ch = 3;
            if(i<3)
            {
                star++;
            }
            else
            {
                star--;
            }
        }
    }
}
