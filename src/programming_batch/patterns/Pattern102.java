package programming_batch.patterns;

public class Pattern102 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        int space = 0;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                if(j==0||j==star-1||i==0||i==line-1)
                {
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }
            space++;
            System.out.println();
        }
    }

}
