package programming_batch.patterns;

public class Pattern111 {
    public static void main(String[] args) {
        int ch1 = 5;
        for (int i = 0; i < 5; i++) {
            int ch2 = ch1;
            for (int j = 0; j < 5; j++) {
                System.out.print(ch2+++" ");
                if(ch2>5)
                {
                    ch2=5;
                }
            }
            ch1--;
            System.out.println();
        }
    }
}
