package programming_batch.patterns;

public class Pattern100 {
    public static void main(String[] args) {
        int line = 9;
        int star = 1;
        int space = 4;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                if(j==0||j==star-1)
                {
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
            }
            System.out.println();
            if(i<4)
            {
                star+=1;
                space--;
            }else {
                star-=1;
                space++;
            }
        }
    }
}
