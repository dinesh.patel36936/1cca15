package programming_batch.patterns;

public class Pattern08 {
    public static void main(String[] args) {
        int line = 4;
        int star = 1;
        int space = 3;
        int ch  = 0;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                if(i>j)
                {
                    System.out.print(ch--);
                }
                else {
                    System.out.print(ch++);
                }
            }
            System.out.println();
            star+=2;
            space--;
        }
    }
}
