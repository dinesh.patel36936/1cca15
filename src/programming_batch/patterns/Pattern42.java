package programming_batch.patterns;

public class Pattern42 {
    public static void main(String[] args) {
        int line = 10;
        int star = 5;
        int ch1 = 1;
        int space = 0;
        for (int i = 0; i < line; i++) {
            int ch2 = ch1;
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print(ch2++ + " ");
            }
            System.out.println();
            if(i<4)
            {
                star--;
                space++;
                ch1++;
            } else if(i>4) {
                star++;
                space--;
                ch1--;
            }
        }

    }
}
