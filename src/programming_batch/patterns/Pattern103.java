package programming_batch.patterns;

public class Pattern103 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        int space = 1;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
               if(j==0||j==star-1||i==0||i==line-1)
               {
                   System.out.print("*");
               }else {
                   System.out.print(" ");
               }
            }
            System.out.println();
            if(i<2)
            {
                star-=2;
                space++;
            }else {
                star+=2;
                space--;
            }
        }
    }
}
