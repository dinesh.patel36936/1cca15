package programming_batch.arrays;

public class Array2 {
    public static void main(String[] args) {
     int[] a1 = {1,2,3,4};
     int[] a2 = {5,6,7,8,9};
     int len1 = a1.length;
     int len2 = a2.length;
     int[] a3 =new int[(len2+len2)-1];
     int j = 0;
        for (int i = 0; i < len1; i++) {
            a3[j] = a1[i];
            j++;
        }

        for (int i = 0; i <len2 ; i++) {
            a3[j] = a2[i];
            j++;
        }
        for(int c:a3)
        {
            System.out.print(c+" ");
        }
    }
}
