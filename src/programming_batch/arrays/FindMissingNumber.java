package programming_batch.arrays;

public class FindMissingNumber {
    public static void main(String[] args) {
        int[] a = {1,2,3,4,6};
        int n = a.length;
        int sum = ((n+1)*(n+2))/2;
        for (int i = 0; i < a.length; i++) {
            sum-=a[i];
        }
        System.out.println(sum);
    }
}
