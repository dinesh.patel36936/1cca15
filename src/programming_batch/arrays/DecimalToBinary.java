package programming_batch.arrays;

public class DecimalToBinary {
    public static void main(String[] args) {
        int[] a = new int[10];
       // String s = "";
        int n = 128;
        int i = 0;
        while(n!=0)
        {
            int r = n%2;
            a[i] = r;
            i++;
            n/=2;
        }
        for (int j = i-1; j >=0 ; j--) {
            System.out.print(a[j]+"");
        }
    }
}
