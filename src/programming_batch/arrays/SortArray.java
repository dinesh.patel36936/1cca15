package programming_batch.arrays;

public class SortArray {
    public static void main(String[] args) {
        int[] a = {2,4,3,5,4,2};
        for (int i = 0; i < a.length; i++) {
            for (int j = i+1; j < a.length ; j++) {
               if(a[i]>a[j])
               {
                   int tmp = a[i];
                   a[i] = a[j];
                   a[j] = tmp;
               }
            }
        }
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i]+" ");
        }
       // System.out.println(a[a.length-2]);
    }
}
