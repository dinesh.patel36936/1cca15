package programming_batch.arrays;

public class array4 {
    public static void main(String[] args) {
        int[] a = {1,2,3,4,5,6,3,2,4,5,1,2,3,4};
        for (int i = 0; i < a.length; i++) {
            for (int j = i+1; j < a.length; j++) {
                if(a[i]>a[j])
                {
                    int t = a[i];
                    a[i] = a[j];
                    a[j] = t;
                }
            }
        }
        for (int i = 0; i < a.length; i++) {
            int count = 1;
            for (int j = i+1; j < a.length; j++) {
                if(a[i]==a[j])
                {
                    count++;
                }
            }

            System.out.println(a[i]+" number occurence is"+count);
            i=i+count-1;
        }
    }
}
