package programming_batch.arrays;

public class Array3 {
    public static void main(String[] args) {
     int[] arr1 = {1,2,3,4};
     int[] arr2 = {5,6,7,8};
     int l1 = arr1.length;
     int l2 = arr2.length;
     int[] arr3 = new int[l1+l2];
     int ch1 =0;
     int ch2 = 0;
        for (int i = 0; i <(l1+l2); i++) {
            if(i%2==0)
            {
                arr3[i]=arr1[ch1++];
            }else {
                arr3[i]=arr2[ch2++];
            }
        }
        for(int a:arr3)
        {
            System.out.print(a+" ");
        }
    }
}
