package programming_batch.arrays;

import java.util.Scanner;

public class ArraySearch {
    public static void main(String[] args) {
        int[] a = {1,2,3,5,3,6,7,8};
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your number:");
        int num = sc.nextInt();
        boolean present = true;
        for (int i = 0; i < a.length; i++) {
            if(num==a[i])
            {
                System.out.println(num+ " is present at "+i+" position.");
                present=false;
            }
        }
        if(present)
        {
            System.out.println("number is not present.");
        }
    }
}
