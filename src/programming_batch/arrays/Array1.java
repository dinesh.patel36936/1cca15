package programming_batch.arrays;

public class Array1 {
    public static void main(String[] args) {
        int[] a = {1,2,3,4,5,6};
        int j = a.length/2;
        for (int i = 0; i < a.length/2; i++) {
            int temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            j++;
        }
        for (int c:a) {
            System.out.print(c+" ");
        }
    }
}
