package programming_batch.arrays;

public class ReverseArray {
    public static void main(String[] args) {
        int[] a = {1,2,3,4,5,6};
        int len = a.length-1;
        for (int i = 0; i <a.length/2 ; i++) {
            int temp = a[i];
            a[i] = a[len];
            a[len] = temp;
            len--;
        }
        for (int c:a)
        {
            System.out.print(c+" ");
        }
    }
}
