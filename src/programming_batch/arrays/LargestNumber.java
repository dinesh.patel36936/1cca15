package programming_batch.arrays;

public class LargestNumber {
    public static void main(String[] args) {
        int[] a = {2,4,3,5,4,2};
        int max = a[0];
        for (int i = 0; i < a.length; i++) {
            if(max<a[i])
            {
                max = a[i];
            }
        }
        System.out.println(max);
    }
}
