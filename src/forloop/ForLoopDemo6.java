package forloop;

public class ForLoopDemo6 {
    public static void main(String[] args) {
        int lines = 5;
        int start  = 1;
        for(int i = 1; i<=lines; i++)
        {
            for(int j = 1; j<=start; j++)
            {
                System.out.print("*");
            }
            System.out.println();
            start++;
        }
    }
}
