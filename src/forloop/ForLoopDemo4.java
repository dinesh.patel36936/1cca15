package forloop;

import java.util.Scanner;

public class ForLoopDemo4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter start point:");
        int start = sc.nextInt();
        System.out.println("Enter end point:");
        int end = sc.nextInt();
        int sum = 0;
        for(int i = start;i<=end; i++)
        {
            if(i%2==0)
            {
                sum+=i;
            }
        }
        System.out.println(sum);
    }
}
