package forloop;

import java.util.Scanner;

public class ForLoopDemo5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter total no. of users :");
        int users = sc.nextInt();

        for(int i = 0;i<users; i++)
        {
            System.out.println("Enter User name:");
            String user = sc.next();
            System.out.println("Welcome "+user);
        }
    }
}
