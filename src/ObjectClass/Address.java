package ObjectClass;

public class Address {
    private  int hno;

    public Address(int hno) {
        this.hno = hno;
    }

    public int getHno() {
        return hno;
    }

    public void setHno(int hno) {
        this.hno = hno;
    }
}
