package ObjectClass;

public class Employee implements  Cloneable{
    private int id;
    private String name;
    private Address add;

    public Address getAdd() {
        return add;
    }

    public void setAdd(Address add) {
        this.add = add;
    }

    public Employee(int id, String name, Address add) {
        this.id = id;
        this.name = name;
        this.add = add;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

     //1. toString() method in object
     @Override
     public String toString() {
        return "Employee id: "+id+"Employee Name: "+name;
        }


    //2. hashCode() method in object

    @Override
    public int hashCode() {
        return id;
    }

    //3. equals()
    @Override
    public boolean equals(Object obj) {
        Employee e = (Employee) obj;
        try{
            if(name.equals(e.getName())&&id==e.getId())
            {
                return true;
            }else {
                return false;
            }
        }catch (Exception e1)
        {
            return false;
        }
    }

    //deep cloning
    //when date member is primitive data in a class then we use shallow cloning
    //and when data member is non-primitive data like class reference of another class in a class then
    //we use deep cloning.
    //In deep cloning we must override clone() method in a class
    @Override
    protected Object clone() throws CloneNotSupportedException {
       Address addr = new Address(add.getHno());
       Employee e = new Employee(id, name, addr);
       return e;
    }
    //4.finallize();
    //5.getclass();
    //6.clone(); types: (i.) ShallowCloning(), (ii.) DeepCloning()

    public static void main(String[] args) throws CloneNotSupportedException {
       Address a = new Address(404) ;
        Employee e = new Employee(1, "Java", a);

       // Employee e2 = new Employee(2, "Python",a2);
       // Employee e3 = e;
       // System.out.println(e);
       // System.out.println(e.toString());
        // System.out.println(e.hashCode());
        // System.out.println(e.equals(e3));
        //System.out.println(e.equals(e2));
        //System.out.println(e.getClass().getName());

        Employee e4 = (Employee) e.clone();
        System.out.println(e4.getId()+" "+e.getName()+" "+e.getAdd().getHno());
        a.setHno(505);
        System.out.println(e.getId()+" "+e.getName()+" "+e.getAdd().getHno());

    }
}
