package angularminds;

public class Pattern10 {
    public static void main(String[] args) {
       int line = 5;
       int star = 1;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                if((i+j)%2!=0)
                {
                    System.out.print(0);
                }
                else {
                    System.out.print(1);
                }
            }
            star++;
            System.out.println();
        }
    }
}
