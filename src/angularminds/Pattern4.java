package angularminds;

public class Pattern4 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        int ch = 5;
        for (int i = 0; i < line; i++) {
            int ch2 = ch;
            for (int j = 0; j < star; j++) {
                System.out.print(ch2++);
                if(ch2>5)
                {
                    ch2 = 5;
                }
            }
            ch--;
            System.out.println();
        }
    }
}
