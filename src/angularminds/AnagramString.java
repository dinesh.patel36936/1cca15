package angularminds;

import java.util.Arrays;
import java.util.Scanner;

public class AnagramString {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your 1st String");
        String s1 = sc.nextLine();
        System.out.println("Enter your 2nd String:");
        String s2 = sc.nextLine();
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();
        if(s1.length()==s2.length())
        {
            char[] ch1 = s1.toCharArray();
            char[] ch2 = s2.toCharArray();

            Arrays.sort(ch1);
            Arrays.sort(ch2);
            if(Arrays.equals(ch1,ch2))
            {
                System.out.println("Anagram Strings");
            }else {
                System.out.println("Not Anagram Strings");
            }
        }
        else {
            System.out.println("Not Anagram Strings");
        }
    }
}
