package angularminds;

public class Pattern12 {
    public static void main(String[] args) {
        int line = 6;
        int star = 6;
        int ch = 1;
        for (int i = 0; i < line; i++) {
            int ch2 = ch;
            for (int j = 0; j < star; j++) {
                if(i==0||j==0||j==star-1||i==line-1)
                {
                    System.out.print("*");
                }
                else if(j>=1&&j<=star-2)
                {
                    System.out.print(ch2++);
                }
            }
            System.out.println();
        }
    }
}
