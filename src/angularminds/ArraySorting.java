package angularminds;

public class ArraySorting {
    public static void main(String[] args) {
        int[] a={1, 2,2,3,4,5,5};
        int n = a.length;
        int [] b = new int[n];
        for(int i = 0; i<n; i++)
        {
            for (int j = i+1; j < n; j++) {
                if(a[i]<a[j])
                {
                    int temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.print(a[i]+" ");
        }
    }
}
