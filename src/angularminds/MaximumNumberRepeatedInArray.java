package angularminds;

import java.util.Scanner;

public class MaximumNumberRepeatedInArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of array:");
        int size = sc.nextInt();
        int[] arr = new int[size];
        System.out.println("Enter the elements of array:");
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }
//        System.out.println("Array elements are:");
//        for (int i = 0; i < size; i++) {
//            System.out.println(arr[i]);
//        }
        int num = maximumNumber(arr,size);
        System.out.println(num);
    }

    private static int maximumNumber(int[] arr, int size) {
        int num = 0;
        int maxTimes = 0;
        for (int i = 0; i < size; i++) {
            int count = 0;
            for (int j = i+1; j < size; j++) {
                if(arr[i]==arr[j])
                {
                   count++;
                }
            }
            if(count>maxTimes)
            {
                maxTimes = count;
                num = arr[i];
            }
        }
        return num;
    }
}
