package angularminds;

public class Pattern5 {
    public static void main(String[] args) {
        int line = 5;
        int star = 1;
        int space = 4;
        for (int j = 0; j < line; j++) {
            for (int i = 0; i < space; i++) {
                System.out.print(" ");
            }
            for (int i = 0; i < star; i++) {
                System.out.print("*");
            }
            System.out.println();
            star++;
            space--;
        }
    }
}
