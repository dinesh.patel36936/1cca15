package oops;
//constructor channing
//superclass
public class Master {
    Master(int a)
    {
        System.out.println(a);
    }
    Master(String s)
    {
        this(25);
        System.out.println(s);
    }
    Master(char c)
    {
        this("java");
        System.out.println(c);
    }

}
