package Serialization;

import java.io.*;

public class Test {
    public static void main(String[] args) {
        Demo d1 = new Demo(1,"geeksforgeeks");
        String filename = "file.ser";

        //serialization
        try{
            //Saving of object in a file
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);

            //method for serializing an object
            out.writeObject(d1);
            out.close();
            file.close();

            System.out.println("Object has been serialized");
        }catch (IOException ex)
        {
            System.out.println("IOException is caught");
        }

        //Deserialization
        Demo d2 = null;
        try{
            //reading the object from a file
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);

            //method for deserializing of object
            d2 = (Demo) in.readObject();
            in.close();
            file.close();

            System.out.println("a = "+d2.a);
            System.out.println("b = "+d2.b);
        }catch(IOException ex)
        {
            System.out.println("IOException is caught");
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println("ClassNotFoundException is caught");
        }
    }
}
