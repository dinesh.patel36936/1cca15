package abstraction;

public class MainApp {
    public static void main(String[] args) {
        Switch s;
        s=new LedLight();
        s.switchOn();
        s.switchOff();

        s=new CflLight();
        s.switchOn();
        s.switchOn();
    }
}
