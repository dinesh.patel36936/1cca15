package abstraction;
//business login implementation
public interface Switch {
    void switchOn();
    void switchOff();
}
