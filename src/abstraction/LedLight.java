package abstraction;

public class LedLight implements Switch{
    @Override
    public void switchOn() {
        System.out.println("LED LIGHT IS SWITCH ON.");
    }

    @Override
    public void switchOff() {
        System.out.println("LED LIGHT IS SWITCH OFF.");
    }
}
