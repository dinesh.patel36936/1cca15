package Interface;
@FunctionalInterface
public interface UPI {
    void transferAmount(double amt);
}
