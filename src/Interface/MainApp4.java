package Interface;

public class MainApp4 {
    public static void main(String[] args) {
        Software s1 = new Software();
        s1.designUI();
        s1.developServerProgram("Python");
        s1.designDatabase("Oracle");
    }
}
