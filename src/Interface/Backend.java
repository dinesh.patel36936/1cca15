package Interface;
//super Interface
@FunctionalInterface
public interface Backend {
    void developServerProgram(String language);
}
