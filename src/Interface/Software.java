package Interface;

public class Software extends FrontEnd implements Backend, Database{
    @Override
    public void developServerProgram(String language) {
        System.out.println("Developing Server Program using "+language);
    }

    @Override
    public void designDatabase(String dbVendor) {
        System.out.println("Designing database using "+dbVendor);
    }
}
