package patterns;

public class Pattern_07 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        int ch1 = 1;
        char ch2 = 'A';
        for(int i = 0; i<line; i++)
        {
            for(int j = 0; j<star; j++)
            {
                if(i%2==0)
                {
                    System.out.print(ch1+++" ");
                }else {
                    System.out.print(ch2+++" ");
                }
            }
            ch1 = 1;
            ch2 = 'A';
            System.out.println();
        }
    }
}
