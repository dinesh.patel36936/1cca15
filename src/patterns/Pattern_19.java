package patterns;

public class Pattern_19 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;
        int num = 1;
        for (int i = 1; i <=line ; i++) {
            for (int j = 1; j <=star ; j++) {
                if(i%2==0)
                {
                    System.out.print("*"+" ");
                }else {
                    System.out.print(num+++" ");
                }
            }
            num = 1;
            star--;
            System.out.println();
        }
    }
}
