package patterns;

public class Pattern_29 {
    public static void main(String[] args) {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 3; j++) {
                if(j==0||i==2||(i>=1&&j==2)||(i==0&&j==1))
                {
                    System.out.print("*");
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
