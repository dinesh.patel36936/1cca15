package patterns;

public class Pattern_04 {
    public static void main(String[] args) {
        int one = 1;
        int zero = 0;
        for(int i = 1; i<=5; i++)
        {
            for(int j = 1;j<=5; j++)
            {
                if(i%2!=0)
                {
                    System.out.print(one+" ");
                }
                else
                {
                    System.out.print(zero+" ");
                }
            }
            System.out.println();
        }
    }
}
