package patterns;

public class Pattern_27 {
    public static void main(String[] args) {
        int star = 1;
        int space = 4;
        int num = 1;
        int num1 = 1;
        for (int i = 0; i <5 ; i++) {
            for (int j = 0; j <space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print(num++);
            }
            for (int j = 1; j < star; j++) {
                System.out.print(num1++);
            }
            System.out.println();
            num=1;
            num1=1;
            star++;
            space--;
        }
    }
}
