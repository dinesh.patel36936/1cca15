package patterns;

public class Pattern_15 {
    public static void main(String[] args) {
        int ch = 1;
        int star = 1;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < star; j++) {
                System.out.print(ch+++ " ");
                if(ch>4)
                {
                    ch = 1;
                }
            }
            star++;
            System.out.println();
        }
    }
}
