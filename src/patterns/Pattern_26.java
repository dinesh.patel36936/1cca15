package patterns;

public class Pattern_26 {
    public static void main(String[] args) {
        int line = 11;
        int star = 1;
        char ch = 'A';
        for (int i = 0; i <line ; i++) {
            for (int j = 0; j <star ; j++) {
                if(ch>'E')
                {
                    ch = 'A';
                }
                System.out.print(ch+++" ");
            }
            System.out.println();
            if(i<3)star++;
            else star--;

        }
    }
}
