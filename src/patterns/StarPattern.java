package patterns;

public class StarPattern {
    public static void main(String[] args) {
        for(int i = 1; i<=10/2; i++)
        {
            for(int j = 1; j<=10/2; j++)
            {
                if(i==j)
                {
                    System.out.print("*");
                }else
                {
                    System.out.print(" ");
                }
            }
            for(int j = 1; j<=10/2; j++)
            {
                if(i+j==10/2)
                {
                    System.out.print("*");
                }else
                {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

    }
}
