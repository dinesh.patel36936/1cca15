package patterns;

public class Pattern_32 {
    public static void main(String[] args) {
        int k = 1;
        for(int i = 0; i<10; i++)
        {
            for (int j = 0; j<20; j++)
            {
                if(i==4||j==4-i||i>4&&(i-j==3))
                {
                    System.out.print("*"+" ");
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
