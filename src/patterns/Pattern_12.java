package patterns;

public class Pattern_12 {
    public static void main(String[] args) {
        char ch = 'A';
        int star = 1;
        for (int i = 0; i<5; i++)
        {
            for(int j = 1; j<=star; j++)
            {
                System.out.print(ch++);
            }
            ch = 'A';
            star++;
            System.out.println();
        }
    }
}
