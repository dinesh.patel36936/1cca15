package patterns;

public class StarPattern1 {
    public static void main(String[] args) {
        int rows = 5;
        int col = 5;
        int star = 1;
        for(int i = 1;i<=rows; i++)
        {

            for(int j = 1;j<=col-i;j++)
            {
                    System.out.print(" ");
            }
            for(int j = 1; j<=star;j++)
            {
                System.out.print("*");
            }
            star++;
            System.out.println();
        }
    }
}
