package patterns;

public class Pattern_03 {
    public static void main(String[] args) {
        int alphabet = 65;
        for(int i = 1; i<=5; i++)
        {
            for(int j = 1;j<=5; j++)
            {
                if(i%2!=0) {
                    System.out.print(j + " ");
                }else {
                    System.out.print((char) (alphabet) + " ");
                    alphabet++;
                }
            }
            alphabet = 65;
            System.out.println();
        }
    }
}
