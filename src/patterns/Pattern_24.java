package patterns;

public class Pattern_24 {
    public static void main(String[] args) {
        int line = 9;
        int star = 1;
        for (int i = 0; i <line ; i++) {
            for (int j = 0; j < star; j++) {
                if(j%2==0) {
                    System.out.print("*" + " ");
                }else {
                    System.out.print(j+" ");
                }
            }
            System.out.println();
            if(i<4)
            {
                star++;
            }else {
                star--;
            }
        }
    }
}
