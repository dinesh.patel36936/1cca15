package patterns;

public class Pattern_05 {
    public static void main(String[] args) {
        int alphabet = 65;
        for(int i = 1; i<=22; i++)
        {
            for(int j = 1;j<=26; j++)
            {
                System.out.print((char)alphabet+" ");
                alphabet++;
            }
            alphabet=65+i;
            System.out.println();
        }
    }
}
