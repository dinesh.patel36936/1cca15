package patterns;

public class StarPattern8 {
    public static void main(String[] args) {
        int row = 5;
        int col = 5;
        int star = 1;
        for (int i = 0; i<row; i++)
        {
            for(int j = 1;j<=col-i;j++)
            {
                System.out.print(" ");
            }
            for(int j = 1; j<=star;j++)
            {
                System.out.print("*");
            }
            for(int k = 2;k<=star; k++)
            {
                System.out.print("*");
            }
            star++;
            System.out.println();
        }
    }
}
