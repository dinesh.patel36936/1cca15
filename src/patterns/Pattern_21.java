package patterns;

public class Pattern_21 {
        public static void main(String[] args) {
            int line = 5;
            int star = 1;
            int num = 1;
            for (int i = 0; i < line; i++) {
                for (int j = 0; j < star; j++) {
                    System.out.print(num+++" ");
                }
                if(i<2)
                {
                    star++;
                }
                else {
                    star--;
                }
                num = 1;
                System.out.println();
            }

        }
    }

