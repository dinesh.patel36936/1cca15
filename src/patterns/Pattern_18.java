package patterns;

public class Pattern_18 {
    public static void main(String[] args) {
        int line = 7;
        int star = 7;
        char ch = 'A';
        for (int i = 0; i <line ; i++) {
            for(int j = 0; j<star; j++)
            {
                System.out.print(ch+" ");
            }
            ch++;
            star-=2;
            System.out.println();

        }
    }
}
