package patterns;

public class Pattern_30 {
    public static void main(String[] args) {
        for (int i = 0; i <5; i++) {
            for (int j = 0; j < 10; j++) {
                if(i==0||j<=5&&j==i||j>5&&j+i==9)
                {
                    System.out.print("*");
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
