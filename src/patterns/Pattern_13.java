package patterns;

public class Pattern_13 {
    public static void main(String[] args) {
        int star = 1;
        char ch = 'A';
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j <star ; j++) {
                System.out.print(ch+" ");
            }
            ch++;
            star++;
            System.out.println();
        }
    }
}
