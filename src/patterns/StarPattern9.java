package patterns;

public class StarPattern9 {
    public static void main(String[] args) {
        int row = 5;
        int col = 5;
        int star = 1;
        int space = 1;
        for(int i = 0; i<row-1; i++)
        {
            for(int j = 1;j<=col-i;j++)
            {
                System.out.print(" ");
            }
            for(int j = 1; j<=star;j++)
            {
                System.out.print(star);
            }
            for(int k = 2;k<=star; k++)
            {
                System.out.print(star);
            }
            star++;
            System.out.println();
        }
        star = 5;
        for(int i = 1;i<=row;i++)
        {
            for(int j = 1; j<=space;j++)
            {
                System.out.print(" ");
            }
            for(int j = 1;j<=star-1; j++)
            {
                System.out.print(star);
            }
            for(int j=1;j<=star; j++)
            {
                System.out.print(star);
            }
            star--;
            space++;
            System.out.println();
        }
    }
}
