package patterns;

public class Pattern_25 {
    public static void main(String[] args) {
      int line = 5;
      int star = 1;
      int space = 4;
        for (int i = 0; i <5 ; i++) {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for(int j = 0; j<star; j++)
            {
                System.out.print("*");
            }
            for (int j = 1; j < star; j++) {
                System.out.print("*");
            }
            System.out.println();
            space--;
            star++;
        }
        space = 1;
        star = 4;
        for(int i = 0; i<5; i++)
        {
            for (int j = 0; j < space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j <star; j++) {
                System.out.print("*");
            }
            for (int j = 1; j <star; j++) {
                System.out.print("*");
            }
            System.out.println();
            star--;
            space++;
        }
    }
}
