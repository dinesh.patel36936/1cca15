package patterns;

public class StarPattern6 {
    public static void main(String[] args) {
        int row = 5;
        int col = 5;
        int star = 1;
        int space = 5;
        for(int i = 1; i<=5; i++)
        {
            for(int j = 1; j<=col-star; j++)
            {
                System.out.print(" ");
            }
            for(int j = 1;j<=star; j++)
            {
                System.out.print("* ");
            }
            System.out.println();
            star++;
            space--;
        }
    }
}
