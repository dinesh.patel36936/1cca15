package patterns;

public class Pattern_14 {
    public static void main(String[] args) {
        int star = 1;
        int k = 1;
        int l = 0;
        for (int i = 1; i <=5 ; i++) {
            for (int j = 1; j <=star ; j++) {
                System.out.print(k+" ");
                k*=2;
            }
            k = l+++1;
            System.out.println();
            star++;
        }
    }
}
