package MethodOverriding;

public class MainApp2 {
    public static void main(String[] args) {
        Dog d1 = new Dog();
        d1.sound();
        Cat c1 = new Cat();
        c1.sound();
    }
}
