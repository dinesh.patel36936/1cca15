package MethodOverriding;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Qty:");
        int qty = sc.nextInt();
        System.out.println("Enter Price");
        double price = sc.nextDouble();
        System.out.println("Select Ecommerce Plateform:");
        System.out.println("1: Flipkart \n2: Amazon");
        int choice = sc.nextInt();
        if(choice==1)
        {
            Flipkart f1 = new Flipkart();
            f1.sellProduct(qty, price);
        }else if(choice==2)
        {
            Amazon a1 = new Amazon();
            a1.sellProduct(qty,price);
        }else
        {
            System.out.println("Invalid Choice");
        }
    }
}
