package MethodOverriding;
//superclass
public class Ecommerce {
    void sellProduct(int qty, double price)
    {
        double total = qty*price;
        System.out.println("Total Cost:"+total);
    }
}
