package MethodOverriding;

public class Cat extends Animal{
    @Override
    void sound() {
        System.out.println("Meow");
    }
}
