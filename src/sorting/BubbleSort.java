package sorting;
public class BubbleSort {
    public static void main(String[] args) {
//        int[] a = {5,3,1,2, 6};
        char[] a = {'a', 'c', 'b'};
        for(int i = 0;i<a.length; i++)
        {
            for(int j = 0; j<a.length; j++)
            {
                if(a[j]>a[j+1])
                {
                    char temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
            }
        }
        for(int i:a)
        {
            System.out.print(i+" ");
        }
    }
}
