package array;

import java.util.Scanner;

public class ArrayDemo4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter total no. of courses: ");
        int n = sc.nextInt();
        String[] courses = new String[n];
        System.out.println("Enter courese name:");
        for(int i = 0; i<n; i++)
        {
            courses[i] = sc.next();
        }
        System.out.println("Selected courses are:");
        for(int i = 0; i<n; i++)
        {
            System.out.println(courses[i]);
        }
    }
}
