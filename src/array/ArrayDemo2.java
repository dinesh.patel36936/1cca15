package array;

public class ArrayDemo2 {
    public static void main(String[] args) {
        int[] data;
        data = new int[5];
        data[0] = 100;
        data[1] = 200;
        data[2] = 300;
        data[3] = 400;
        data[4] = 500;

        for(int i = 0; i<5; i++)
        {
            System.out.println(data[i]);
        }
    }
}
