package array;
import java.util.*;
public class ArrayDemo1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter total no of courses:");
        int n = sc.nextInt();
        int[] marks = new int[n];
        int sum = 0;
        for(int i = 0; i<n; i++)
        {
            marks[i]=sc.nextInt();
            sum+=marks[i];
        }
        System.out.println(sum);
        System.out.println(sum/n);
    }
}
