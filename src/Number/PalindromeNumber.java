package Number;

public class PalindromeNumber {
    public static void main(String[] args) {
        int n = 123;
        int t = n;
        int reverseNumber = 0;
        while(n>0)
        {
            int r = n%10;
            reverseNumber= reverseNumber*10+r;
            n/=10;

        }
        System.out.println((t==reverseNumber)?"Palindrome":"Not Palindrome");
    }
}
