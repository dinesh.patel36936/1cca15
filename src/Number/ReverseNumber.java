package Number;

public class ReverseNumber {
    public static void main(String[] args) {
        int n = 12345;
        int reverseNumber = 0;
        while(n>0)
        {
            int r = n%10;
            reverseNumber= reverseNumber*10+r;
            n/=10;

        }
        System.out.println(reverseNumber);
    }
}
