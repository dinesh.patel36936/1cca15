package operatoroverloading;

public class MainApp2 {
    public static void main(String[] args) {
        mirrorSentences("hello world i am dinesh");
    }

    private static void mirrorSentences(String str) {
        String s ="";
        String[] ch = str.split(" ");
        int len = ch.length;
        for(int i = len-1;i>0; i--)
        {
            s+=ch[i]+" ";
        }
        s+=ch[0];
        System.out.println(s);
    }
}
