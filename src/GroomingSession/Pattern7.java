package GroomingSession;

public class Pattern7 {
    public static void main(String[] args) {
        int line = 4;
        int ch = 1;
        for (int i = 0; i < line; i++) {
            int  ch1 = ch;
            for (int j = 0; j <= line; j++) {
                System.out.print(ch1++);
            }
            ch++;
            System.out.println();
        }
    }
}
