package GroomingSession.string;

public class ReverseSentenceInEachWord {
    public static void main(String[] args) {
        String s1 = "my name is core java";
        String str = "";
        for (int i = s1.length()-1; i >=0 ; i--) {
            str+= s1.charAt(i);
        }
        String[] ch =str.split(" ");
        for (int i = ch.length-1; i >=0 ; i--) {
            System.out.print(ch[i]+" ");
        }
    }
}
