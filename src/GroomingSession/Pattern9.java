package GroomingSession;

public class Pattern9 {
    public static void main(String[] args) {
        int star = 1;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(star+++" ");
                if(star>5)
                {
                    star = 1;
                }
            }
            star++;
            System.out.println();
        }
    }
}
