package GroomingSession.Array;

public class MergeTwoArray {
    public static void main(String[] args) {

        int[] arr1 = {10, 20, 30, 40, 50};
        int[] arr2 = {60, 70, 80, 90, 100};

        int len1 = arr1.length;
        int len2 = arr2.length;
        int[] arr3 = new int[len1+len2];
        int count = 0;
        for (int i = 0; i < arr1.length; i++) {
            arr3[count++] = arr1[i];
        }
        for (int i = 0; i < arr2.length; i++) {
            arr3[count++] = arr2[i];
        }
        for(int a:arr3)
        {
            System.out.print(a+" ");
        }
    }
}
