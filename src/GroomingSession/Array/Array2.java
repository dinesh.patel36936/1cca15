package GroomingSession.Array;

public class Array2 {
    public static void main(String[] args) {
        int[] arr1 = {1,3,5,7};
        int[] arr2 = {2,4,6,8,10};
        int ch1 = 0;
        int ch2 = 0;
        int l1 = arr1.length;
        int l2 = arr2.length;
        int[] arr3=new int[l1+l2];
        for (int i = 0; i <(l1+l2) ; i++) {
            if(i%2==0&&ch1<l1)
            {
                arr3[i]=arr1[ch1++];
            }
            else if(ch2<l2) {
                arr3[i]=arr2[ch2++];
            }
        }
        for (int i = 0; i <arr3.length; i++) {
            System.out.println(arr3[i]);
        }
    }
}
