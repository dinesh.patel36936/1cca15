package GroomingSession.Array;

public class MergeTwoArrays {
    public static void main(String[] args) {
        int arr1[] = {1,3,5,7};
        int arr2[] = {2,4,6,8,10};
        int len1 = arr1.length;
        int len2 = arr2.length;
        int arr3[] = new int[len1+len2];
        int ch1=0;
        int ch2=1;
        for (int i = 0; i <=arr3.length; i++) {
            if(ch1<arr1.length) {
                arr3[ch1] = arr1[i];
                ch1+=2;
            }
            if(ch2<arr2.length) {
                arr3[ch2] = arr2[i];
                ch2 += 2;
            }
        }
        for (int a:arr3)
        {
            System.out.print(a+" ");
        }
    }
}
