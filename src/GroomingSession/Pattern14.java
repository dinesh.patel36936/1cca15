package GroomingSession;

public class Pattern14 {
    public static void main(String[] args) {
        int line = 6;
        int ch = 1;
        int star = 2;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                if(i%2!=0)
                {
                    System.out.print(ch);
                }else {
                    System.out.print("*");
                }
            }
            if(i%2!=0)
            {
                ch++;
            }
            System.out.println();
            star++;
        }
    }
}
