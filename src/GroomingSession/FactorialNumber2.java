package GroomingSession;

public class FactorialNumber2 {
    public static void main(String[] args) {
        int n = fact(5);
        System.out.println(n);
    }

    private static int fact(int i) {
        if(i==0)
        {
            return 1;
        }
        return i*fact(i-1);
    }
}
