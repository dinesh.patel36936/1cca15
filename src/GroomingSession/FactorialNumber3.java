package GroomingSession;

public class FactorialNumber3 {
    public static void main(String[] args) {
        for (int i = 1; i <=100; i++) {

            int n = i;
            long fact = 1L;
            for (int j = 1; j <= n; j++) {
                fact = fact * j;
            }
            System.out.println(i+" factorial number is "+fact);
        }
    }
}
